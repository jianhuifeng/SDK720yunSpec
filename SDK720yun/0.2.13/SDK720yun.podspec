Pod::Spec.new do |s|
  s.name             = 'SDK720yun'
  s.summary          = 'SDK720yun'
  s.version          = '0.2.13' 
 
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://720yun.com'
  s.author           = { '720yun.com' => 'jianhuifeng@720yun.com' }
  s.source           = { :git => 'ssh://git_720yun_app@211.151.0.153:16100/data/othink/git/720yun_ios_sdk.git', :tag => s.version.to_s }
  s.license = 'MIT'

  s.ios.deployment_target = '9.0'

  #s.source_files = 'SDK720yun/Classes/**/*'


  s.default_subspec = 'Core'
  

  s.subspec 'Core' do |core|
    core.source_files = 'SDK720yun/Classes/Core/**/*'
  end

  s.subspec 'PurePanoView' do |spv|
    spv.source_files = 'SDK720yun/Classes/PurePanoView/**/*'
    spv.exclude_files = 'SDK720yun/Classes/TTT/*'
    spv.dependency 'SDK720yun/Core'
    #spv.resource = 'SDK720yun/Assets/Player/**/*'
    spv.resource_bundles = {
      'SDK720yun' => ['SDK720yun/Assets/Player/**/*']
    }
  end


  
  #s.resource_bundles = {
    #'SDK720yun_Player' => ['SDK720yun/Assets/Player/**/*'],
    #'SDK720yun_TTT' => ['SDK720yun/Classes/TTT/**/*']
  #}

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
